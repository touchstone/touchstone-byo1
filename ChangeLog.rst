=========
ChangeLog
=========

v0.2.0 2020-12-16
=================

* Support use of configuration files.
* Have explicit commands to `show` benchmark details and `run` the benchmark.
* Add `results` command to review benchmark results.
* Display benchmark's README with `show` command, if it exists.

v0.1.1 2020-12-03
=================

* Add ChangeLog and documentation.

v0.1.0 2020-11-29
=================

* Initial release supporting basic functionality: list steps, execute all or an
  individual step.
