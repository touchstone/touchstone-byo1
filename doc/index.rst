==============================================
Touchstone - Bring Your Own: Model 1 Benchmark
==============================================

This project is a framework for executing a set of user defined benchmark.  A
user defined benchmark is composed of a set of executable files that the
framework will step through and execute.

It is the responsibility of the supplied executable files to perform any tasks
related to setting up, executing, post processing, and clean up.

Structure of a User Defined Benchmark
=====================================

This is the directory and file structure of the **test** benchmark provided in
the **examples** directory::

    examples/test/
    |-- 00stage1
    |   |-- 00step1
    |   `-- 01step2
    `-- 01stage2
        `-- 00step

The granularity of a benchmark can be divided up into **stages**.  A **stage**
can be further divided up into multiple **steps**.

The **test** benchmark is composed of two stages, *00stage1* and *01stage2*.
*00stage1* consists of two steps *00step1* and *00step2*.  *01stage2* consists
of only one step *00step*.

The filename of the stages and steps are expected to be in the form *XXNAME*,
where *XX* is a two digit number that dictates the order that each stage and
step are executed.  The *NAME* can be any string that is intended to be a
shorthand identifier to describe each stage and step.

If there is a subdirectory that should not be executed, create a file
`.donotexecute` in that directory.  Otherwise the hardness will attempt to
execute any files in that directory.

Benchmark Specific Environment Variables
----------------------------------------

Benchmark specific environment variables can be defined in a *profile* file at
the top level of the benchmark directory.  Remember that these variables need
to be exported in order to be used by the benchmark scripts.  For example::

    export MYVAR="value"

Usage
=====

The stages and steps of a benchmark can be reviewed by executing the framework
with the ``show`` command::

    % src/scripts/tsbyo1 show examples/test
    STAGES:
      00stage1
      01stage2

Execute the entire benchmark::

    % src/scripts/tsbyo1 -o /tmp/results run examples/test
    RUNNING: test
    STAGES:
      00stage1
      01stage2
    STAGE 00stage1
      STEPS:
        00step1
        01step2
    STAGE 01stage2
      STEPS:
        00step

Execute a single stage, *stage1* of the benchmark (the full *XXNAME* or or the
number *XX* can also be used)::

    % src/scripts/tsbyo1 -o /tmp/results run examples/test stage1
    STAGE 00stage1
      STEPS:
        00step1
        01step2

Indivdual steps may also be executed by specifying the step after the stage in
a similar manner.

Configuration
=============

See ``examples/config`` for available configuration options.

The configuration file will first load options from
``${HOME}/.config/touchstone/config``, then will override any options if an
configuration file is specified by the ``-c`` command line option, and finally
any option further specified on the command line will take precedence.
