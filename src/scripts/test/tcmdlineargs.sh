#! /bin/sh
#
# Copyright 2020 PostgreSQL Global Development Group
#

oneTimeSetUp() {
	TOPDIR=`dirname $0`
	TOPDIR="${TOPDIR}/../../.."
}

setUp() {
	# Need to create a new results directory per test because they finish so
	# fast that the automatically generated name by time stamp will clash.
	CONFIGFILE=`mktemp`
	RESULTSDIR=`mktemp -d`
	echo "RESULTS=$RESULTSDIR" > $CONFIGFILE
}

tearDown() {
	rm -rf $CONFIGFILE $RESULTSDIR
}

testHelp()
{
	${TOPDIR}/src/scripts/tsbyo1 -h
	assertEquals "Return code from help" 0 $?
}

testExistingConfigFile()
{
	${TOPDIR}/src/scripts/tsbyo1 -c $CONFIGFILE show ${TOPDIR}/${BENCHMARK}
	assertEquals "Return code from existing config file" 0 $?
}

testInvalidBenchmak()
{
	BENCHMARK=`mktemp -du`
	${TOPDIR}/src/scripts/tsbyo1 show ${TOPDIR}/${BENCHMARK}
	assertEquals "Return code from invalid benchmark directory" 1 $?
}

testListResults()
{
	${TOPDIR}/src/scripts/tsbyo1 -c $CONFIGFILE run ${TOPDIR}/examples/test
	assertEquals "Run all stages" 0 $?

	${TOPDIR}/src/scripts/tsbyo1 -c $CONFIGFILE results list
	assertEquals "List test results" 0 $?
}

testNoArguments()
{
	${TOPDIR}/src/scripts/tsbyo1
	assertEquals "Return code from no arugments" 1 $?
}

testNonExistingConfigFile()
{
	NOCONFIGFILE=`mktemp -u`
	${TOPDIR}/src/scripts/tsbyo1 -c $NOCONFIGFILE show ${TOPDIR}/${BENCHMARK}
	assertEquals "Return code from non-existing config file" 1 $?
}

testRunAllStages()
{
	${TOPDIR}/src/scripts/tsbyo1 -c $CONFIGFILE run ${TOPDIR}/examples/test
	assertEquals "Run all stages" 0 $?

	ROWS=`wc -l ${RESULTSDIR}/*/time.csv | cut -d " " -f 1`
	assertEquals "Expected number of lines in time.csv" 4 $ROWS
}

testRunStageByFullName()
{
	${TOPDIR}/src/scripts/tsbyo1 -c $CONFIGFILE run \
			${TOPDIR}/examples/test 01stage2
	assertEquals "Run stage by full name successfully" 0 $?
}

testRunStageByNameOnly()
{
	${TOPDIR}/src/scripts/tsbyo1 -c $CONFIGFILE run ${TOPDIR}/examples/test \
			stage1
	assertEquals "Run stage by name successfully" 0 $?
}

testShowLatestResults()
{
	${TOPDIR}/src/scripts/tsbyo1 -c $CONFIGFILE run ${TOPDIR}/examples/test
	assertEquals "Run all stages" 0 $?

	${TOPDIR}/src/scripts/tsbyo1 -c $CONFIGFILE results
	assertEquals "Display latest results" 0 $?
}

testValidArgumentsShow()
{
	${TOPDIR}/src/scripts/tsbyo1 show ${TOPDIR}/examples/test
	assertEquals "Return code from valid arguments" 0 $?
}

. `which shunit2`
